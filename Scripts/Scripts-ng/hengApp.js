'use strict';

var hengApp = angular.module('hengApp', ['ngRoute']);

hengApp.config(function($routeProvider){

$routeProvider
    .when('/xoxo',{
    templateUrl:'Pages/xoxo.html'
    , controller: 'xoxoController'
    
    })
    .when('/xxxx',{
        templateUrl:'Pages/xxxx.html'
        , controller: 'xxxxController'
        
        })
    .when('/callApi',{
        templateUrl:'Pages/callApi.html'
        , controller: 'callApiController'
            
        })
    .when('/formatDate',{
    templateUrl:'Pages/formatDate.html'
    ,controller: 'formatDateController'

    })
.otherwise({redirectTo: '/'});
});
