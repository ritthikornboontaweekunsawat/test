hengApp.controller('xoxoController',function($scope){
    $scope.pattern_N= 3;
    $scope.stringResult = [];
    $scope.drawPattern = function(){
        var result = [];
        var item="";
        var toggie = false;
        for(var loop=1;loop<=$scope.pattern_N;loop++){
            item = "X";
            toggie = false;
            for(var loop2=1 ;loop2 <=$scope.pattern_N;loop2++){
                if(loop>loop2){
                item = "O"+item;
                }
                else if(loop<loop2){
                    if(toggie){
                        item = item +"X"
                        }
                    else{
                            item = item +"O"
                        }
                    toggie = !toggie;
                }
                
            }

        // add reverse 
        item = item+$scope.reverseString(item,1);
        result.push(item);
        }
        // Copy half row
        for(var loop1 = result.length -1 ;loop1 > 0;loop1--){
            result.push(result[loop1-1]);
        }
        
        $scope.stringResult = result;        
    }

    $scope.reverseString = function(strInvert,skipCharecter){
        var newString = "";
        // skip 1 charecter
          for (var loop = strInvert.length - (1+skipCharecter); loop >= 0; loop--) {
              newString += strInvert[loop];
          }
          return newString;
    }

})