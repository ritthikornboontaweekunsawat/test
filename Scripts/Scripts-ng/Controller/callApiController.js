hengApp.controller('callApiController', function ($scope,$http) {
   
    $scope.info = [];
    $scope.baseURL = 'https://jsonplaceholder.typicode.com/users';
    $scope.GetData = function(){

    var paramsObj = {  id:$scope.inputID};
    $http({
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/users',
        params: paramsObj,
        headers: { "Content-Type": "application/json" }
        }).then(function (data, status) {
            $scope.info =   data.data;
        },function(error){

        });
    }

})