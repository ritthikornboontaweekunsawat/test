hengApp.controller('xxxxController',function($scope){
    $scope.pattern_N= 3;
    $scope.stringResult = [];
    $scope.drawPattern = function(){
        var result = [];
        var item="";
        var newitem="";
        var evenNumber = $scope.pattern_N % 2  > 0 ? false:true;

        for(var loop=1;loop<=$scope.pattern_N;loop++){
            item = item+"X";
            newitem = newitem+"_";
        }
        // add Center
        result.push(item);
        // add even
        if(evenNumber){
        result.push(item);
        }
        var countCenter = result.length;
         var itemNotCenter =newitem;
        for(var loop= ($scope.pattern_N-countCenter)/2;loop >=1 ;loop--){
            itemNotCenter =newitem;
                for(var loop2=1;loop2<=loop;loop2++){
                    itemNotCenter = itemNotCenter.replaceAt(loop2-1,"X");
                    itemNotCenter = itemNotCenter.replaceAt($scope.pattern_N-loop2,"X");
                }
                result.push(itemNotCenter); 
                result.splice(0, 0, itemNotCenter)
        }           
        
        $scope.stringResult = result;        
    }

    String.prototype.replaceAt = function(index, replacement) {
        return this.substr(0, index) + replacement + this.substr(index + replacement.length);
    }



})