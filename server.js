var express = require('express');
var path = require('path');
var app = express();


app.use(express.static(__dirname))
app.get('*',function(req,res){
    res.sendFile(path.join(__dirname+'/MasterPages/index.html'));
})

app.listen(process.env.PORT || 8080 , function(){
    console.log("Server is running");
});
